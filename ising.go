package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"ising/alg"
	"math/rand"
	"time"
)

func main() {
	energy, magnetization := MonteCarloRun(8, 600, 5000)

	energyJson, _ := json.Marshal(energy)
	magnetizationJson, _ := json.Marshal(magnetization)

	_ = ioutil.WriteFile("8x8-energy.json", energyJson, 0644)
	_ = ioutil.WriteFile("8x8-magnetization.json", magnetizationJson, 0644)
}

func MonteCarloRun(n, runs, steps int) (map[string]map[string][]float32, map[string]map[string][]float32) {
	rand.Seed(time.Now().UnixNano())

	simulation := new(alg.Simulation)
	simulation.N = n
	simulation.Steps = steps

	var energyMap = map[string]map[string][]float32{}
	var magnetizationMap = map[string]map[string][]float32{}

	for i := 0; i < runs; i++ {
		simulation.SetInitialState()
		energyMap[fmt.Sprint(i)] = map[string][]float32{}
		magnetizationMap[fmt.Sprint(i)] = map[string][]float32{}

		for temperature := 1.0; temperature < 4; temperature += 0.05 {
			simulation.SetInitialEnergy()
			simulation.SetInitialMagnetization()

			simulation.MetropolisHastings(float32(temperature))

			energyMap[fmt.Sprint(i)][fmt.Sprint(temperature)] = simulation.Energy[4000:]
			magnetizationMap[fmt.Sprint(i)][fmt.Sprint(temperature)] = simulation.Magnetization[4000:]
		}
	}

	return energyMap, magnetizationMap
}
