import json
import os
from concurrent import futures

import matplotlib.pylab as plt
import numpy as np


class IsingViz:
    def __init__(self, N, magnetizationFile: str, energyFile: str):
        self.N = N
        self.energy, self.temperature = self.loadContent(energyFile)
        self.magnetization, self.temperature = self.loadContent(magnetizationFile)

    @staticmethod
    def loadContent(fileName: str) -> tuple[np.array, np.array]:
        with open(fileName) as file:
            result = json.load(file)

        temperature = np.array([float(i) for i in result['0'].keys()])
        content = np.zeros((len(result), len(result['0']), len(result['0']['1'])))

        for run, sim in result.items():
            for counter, points in enumerate(sim.values()):
                content[int(run), counter] = np.array(points) 
            
        return content, temperature

    def magnetizationPlot(self, ax=None):

        with plt.style.context('ggplot'):
            if ax is None:
                fig, ax = plt.subplots(figsize=(12,8)) 
    
            for i in abs(self.magnetization.mean(axis=2)):
                ax.plot(self.temperature, i, 'k-', lw=1, alpha=0.1)

            ax.plot(self.temperature, abs(self.magnetization.mean(axis=2)).mean(axis=0), 'bo', ms=16)
            plt.xlabel("T", fontdict={'size': 20})
            plt.ylabel(r"$\langle M \rangle$", fontdict={'size': 20})
            plt.tick_params(labelsize=20)
            plt.tight_layout()

    def energyPlot(self, ax=None):

        with plt.style.context('ggplot'):
            if ax is None:
                fig, ax = plt.subplots(figsize=(12,8)) 
    
            for i in self.energy.mean(axis=2):
                ax.plot(self.temperature, i, 'k-', lw=1, alpha=0.1)

            ax.plot(self.temperature, self.energy.mean(axis=2).mean(axis=0), 'bo', ms=16)
            plt.xlabel("T", fontdict={'size': 20})
            plt.ylabel(r"$\langle E \rangle$", fontdict={'size': 20})
            plt.tick_params(labelsize=20)
            plt.tight_layout()

    def susceptibilityPlot(self, ax=None):

        with plt.style.context('ggplot'):
            if ax is None:
                fig, ax = plt.subplots(figsize=(12,8)) 

            ax.plot(self.temperature, self.N*self.N*self.magnetization.var(axis=2).mean(axis=0)/self.temperature, lw=3, alpha=0.8, label=self.N)
            plt.xlabel("T", fontdict={'size': 20})
            plt.ylabel(r"$\langle \chi \rangle$", fontdict={'size': 20})
            plt.tick_params(labelsize=20)
            plt.legend(fontsize=20)
            plt.tight_layout()

    def heatCapacityPlot(self, ax=None):

        with plt.style.context('ggplot'):
            if ax is None:
                fig, ax = plt.subplots(figsize=(12,8)) 

            ax.plot(self.temperature, self.energy.var(axis=2).mean(axis=0)/(self.N*self.temperature**2), lw=3, alpha=0.8, label=self.N)
            plt.xlabel("T", fontdict={'size': 20})
            plt.ylabel(r"$\langle C \rangle$", fontdict={'size': 20})
            plt.tick_params(labelsize=20)
            plt.legend(fontsize=20)
            plt.tight_layout()