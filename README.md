# Ising

Go implementation of the Metropolis-Hastings Monte Carlo algorithm to the Ising model. It also contains a Python visualization library.

## TODO

- [] Generalize to run on different lattices. Use go interface to do so
- [] User friendly command line interface
- [] Improve Python visualization module
- [] Write documentation

## Images

### Magnetization

Magnetization curve for a square lattice of size 24x24, together with the Onsager analytical solution (red curve). The blue dots are an
average of 600 different runs.

![Ising magnetization](/images/magnetization.png)

### Heat capacity

Heat capacity for an Ising model on a square lattice of different sizes.


![Ising heat capacity](/images/heat_capacity.png)

### Magnetic susceptibility

Magnetic susceptibility for an Ising model on a square lattice of different sizes.

![Ising heat capacity](/images/susceptibility.png)