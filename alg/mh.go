package alg

import (
	"math"
	"math/rand"
)

type Simulation struct {
	N             int
	Steps         int
	Spins         [][]int8
	Magnetization []float32
	Energy        []float32
}

func (s *Simulation) SetInitialState() {
	s.Spins = make([][]int8, s.N)
	for i := 0; i < s.N; i++ {
		s.Spins[i] = make([]int8, s.N)
		for j := 0; j < s.N; j++ {
			s.Spins[i][j] = 1
		}
	}
}

func (s *Simulation) SetInitialMagnetization() {
	s.Magnetization = make([]float32, s.Steps)
	s.Magnetization[0] = get_average_magnetization(s.Spins)
}

func (s *Simulation) SetInitialEnergy() {
	s.Energy = make([]float32, s.Steps)

	for i := 0; i < s.N; i++ {
		for j := 0; j < s.N; j++ {
			neigh := s.Spins[mod(i+1, s.N)][j] + s.Spins[mod(i-1, s.N)][j] + s.Spins[i][mod(j+1, s.N)] + s.Spins[i][mod(j-1, s.N)]
			s.Energy[0] -= float32(s.Spins[i][j] * neigh / 2)
		}
	}
}

func (s *Simulation) MetropolisHastings(temperature float32) {
	for step := 1; step < s.Steps; step++ {

		s.Energy[step] = s.Energy[step-1]
		s.Magnetization[step] = s.Magnetization[step-1]

		for k := 0; k < int(math.Pow(float64(s.N), 2)); k++ {
			i, j := random_spins(s.N)
			delta := energy_delta(s.Spins, i, j)

			if delta < 0 || rand.Float64() < math.Exp(-float64(delta/temperature)) {
				s.Spins[i][j] = -s.Spins[i][j]
				s.Energy[step] += delta
				s.Magnetization[step] += float32(2*s.Spins[i][j]) / float32(math.Pow(float64(s.N), 2))
			}
		}
	}
}

func energy_delta(Spins [][]int8, i int, j int) float32 {
	n := len(Spins)
	neigh := Spins[mod(i+1, n)][j] + Spins[mod(i-1, n)][j] + Spins[i][mod(j+1, n)] + Spins[i][mod(j-1, n)]
	return 2 * float32(Spins[i][j]*neigh)
}

func mod(a, b int) int {
	return (a%b + b) % b
}

func get_average_magnetization(Spins [][]int8) float32 {
	var average float32 = 0.0
	for _, row := range Spins {
		for _, spin := range row {
			average += float32(spin)
		}
	}
	return average / float32(math.Pow(float64(len(Spins)), 2))
}

func random_spins(n int) (int, int) {
	return rand.Intn(n), rand.Intn(n)
}
